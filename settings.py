#!/usr/bin/python
# -*- encoding: utf-8 -*-
import os
__author__ = 'Rudolf'

""" General settings """

FILE_PATH = "C:\Users\R\Desktop\D00"

PAPER_FILE_PATH = FILE_PATH+"\paper\paper"
PAPER_FILE_ENDING = ".paper"

CITED_BY_FILE_PATH = FILE_PATH+"\citedby\citedby"
CITED_BY_FILE_ENDING = ".cb"

CSV_FILE_OUTPUT = "C:\csv"

CSV_MAX_ROWS = 50000

# split the where select query after x percent of all isi´s,
# 0.05 means that 100% of data will be handled in chunks of 5% the length of the whole data
# so instead of 1 query with 100%, we get 20 queries with each 5% of the data
QUERY_WHERE_ISI_SPLIT_AFTER_PERCENT = 0.05

""" Neo4j Settings """
NODE_INDEX_NAME = "INDEX_ISI"
NODE_INDEX_KEY_NAME = "ISI"

AUTHOR_NODE_INDEX_NAME = "INDEX_AUTHOR"
AUTHOR_NODE_INDEX_KEY_NAME = "AUTHOR"

NEO4J_SERVER_DATA_URL = "http://localhost:7474/db/data/"

ARRAY_JOIN_CHARACTER = "|"

""" Affiliation Settings """
#e.g.: for a affiliation = [Agr Res Org, Inst Plant Sci, Dept Fruit Tree Sci, PO Box 6, IL-50250 Bet Dagan, Israel], only the last X entries will be taken, the other will be thrown away
AFFILIATION_SPLIT_DEPTH = 3
#value from 0.0 to x which describes how different 2 affiliation strings can be;
## 0.0 = 0% distance, 1.0 = 100% distance, ... 3.0 300% distance
#the percent similarity value is based on: distance to length proportions
#exactly: (float(distance)/(len(a)+1.0) + (float(distance)/(len(b)+1.0))) /2.0 <= AFFILIATION_SIMILARITY_VALUE
AFFILIATION_SIMILARITY_VALUE = 0.3

REMOTE_PROJECT_DIRECTORY = '/tmp/idp'

"""
Load settings_remote.py if we are on a linux type host
"""
try:
    import platform
    if "inux" in str(platform.platform()):
        from settings_remote import *
except ImportError:
    pass