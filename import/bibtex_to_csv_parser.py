import csv
import os
import shutil
import time
import settings
from utils import utils

__author__ = 'Rudolf'

class Article:
    """
    Domain class for Article, contains variables to hold the Values of an Article
    and methods to convert from bibtex_entry to an Article instance
    """

    def __init__(self, id = None):
        self.id = id
        self.isi = None
        self.title = None
        self.issn = None
        self.journal = None
        self.year = None
        self.number = None
        #self.pages = None
        self.publisher = None
        #self.address = None
        self.affiliation = None
        self.doi = None
        self.type = None
        self.keywords = None
        self.keywords_plus = None
        self.number_of_cited_references = None
        #self.author = None
        self.times_cited = None

    @staticmethod
    def format_article_entries(article):
        """
        This method formats the article entries into a neo4j_import readable manner:
        All arrays are concatenated with settings.ARRAY_JOIN_CHARACTER
        All other values are converted to string and truncated
        :param article:
        :return:
        """
        #convert list to strings if needed and join them with specified settings.ARRAY_JOIN_CHARACTER
        #else convert them to normal strings
        for key,entry in article.__dict__.items():
            if type(entry) is list:
                article.__dict__[key] = settings.ARRAY_JOIN_CHARACTER.join([str(e).strip() for e in entry])
            elif entry is None:
                article.__dict__[key] = ''
            else:
                article.__dict__[key] = str(entry).strip()

        return article

    @staticmethod
    def get_instance_by_bibtex(bibtex_entry):
        """
        tricky method =)
        We check dynamically which member variables our own class Article has := (all self."varname" definitions in __init__()).
        After this we check whether the bibtex_entry has any value for this member variable, if yes take this value, else: set to None.
        """

        id = bibtex_entry["id"].split(":")[1].strip()

        article = Article(str(id))

        #read dynamically our own class properties and fill them with da from bibtex_entry
        for key in article.__dict__.keys():
            if key.replace("_", "-") in bibtex_entry:
                entry = bibtex_entry[key.replace("_", "-")]
                article.__dict__[key] = entry

        article.id = int(id)
        article.isi = int(id)

        article = Article.format_article_entries(article)

        return article


class Author:
    """
    Domain class for Author, contains variables to hold the Values of an Author
    and methods to convert from bibtex_entry to an Author instance
    """
    id_counter = 0
    #map holds: authors_data[author_name][affiliation] = id
    authors_data = {}

    def __init__(self, id = None, name=None, affiliation=None):
        self.id = id
        self.name = name
        self.affiliation = affiliation


    @staticmethod
    def get_lowest_similarity(name, affiliation):
        """
        gets the lowest similartiy index for pair: name, affiliation
        :param name:
        :param affiliation:
        :return:
        """
        class SimilarityIndex:
            def __init__(self, affiliation, similarity_distance):
                self.affiliation = affiliation
                self.similarity_distance = similarity_distance

        #create author in map if None exists
        if not name in Author.authors_data:
            Author.authors_data[name] = {}

        #create affiliation if None exists
        if not len(Author.authors_data[name]) > 0:
            Author.authors_data[name][affiliation] = Author.id_counter
            Author.id_counter += 1

        #calculate all similarity indexes for given author
        sorted_affiliation_similarity_indices = []
        for affiliation_key in Author.authors_data[name]:
            similarity_distance = utils.similarity_index(affiliation,affiliation_key)

            affiliation_similarity_index = SimilarityIndex(affiliation_key,similarity_distance)
            sorted_affiliation_similarity_indices.append(affiliation_similarity_index)

        ##sort by highest similarity index
        sorted_affiliation_similarity_indices = sorted(sorted_affiliation_similarity_indices, key=lambda entry: entry.similarity_distance)

        sorted_affiliation_similarity_indices.reverse()
        return sorted_affiliation_similarity_indices.pop()

    @staticmethod
    def get_instance_by_name(name, affiliation):
        """
        create Author instance from name & affiliation
        use similarity index to ensure
        :param name:
        :param affiliation:
        :return:
        """

        #TODO: fix?
        #Affiliation is now NONE
        affiliation = None

        name = str(name).strip()
        affiliation = str(affiliation).strip()

        lowest_similarity_index = Author.get_lowest_similarity(name,affiliation)

        #probe with settings
        if lowest_similarity_index.similarity_distance <= settings.AFFILIATION_SIMILARITY_VALUE:
            id = Author.authors_data[name][lowest_similarity_index.affiliation]
            return Author(int(id), name, lowest_similarity_index.affiliation)
        else:
            #create new author
            Author.authors_data[name][affiliation] = Author.id_counter
            Author.id_counter += 1

            id =  Author.authors_data[name][affiliation]
            return Author(int(id), name,affiliation)

    @staticmethod
    def get_affiliation_of_author(name, affiliations):
        """
        create second name alternative
        transform: "Maier, Josef" into "Maier, J"
        Reason: many affiliations just state it like follows: "Maier, J" without full first name
        """
        try:
            name_split = name.split(",")
            name_alt = name_split[0].strip()+", "
            for i in range(1, len(name_split)):
                name_alt = name_alt + name_split[i].strip()[0].upper()
        except Exception as e:
            name_alt = None

        for affiliation in affiliations:
            if name in affiliation:
                return ", ".join(affiliation.split(",")[-settings.AFFILIATION_SPLIT_DEPTH:]).strip()
            elif name_alt:
                if name_alt in affiliation:
                    return ", ".join(affiliation.split(",")[-settings.AFFILIATION_SPLIT_DEPTH:]).strip()

        return None

    @staticmethod
    def get_instances_by_bibtex(bibtex_entry):
        """
        gets Author instance from bibtex entry
        """
        authors = []
        for author_name in bibtex_entry["author"]:
            if "affiliation" in bibtex_entry:
                affiliation = Author.get_affiliation_of_author(author_name, bibtex_entry["affiliation"])
            else:
                affiliation = None

            author = Author.get_instance_by_name(author_name, affiliation)

            authors.append(author)

        return authors

class ArticleHasAuthor:
    """
    domain class for Article-has->Author
    """
    def __init__(self, article_id, author_id):

        self.article_id = int(article_id)
        self.author_id = int(author_id)



class ArticleHasArticle:
    """
    domain class for Article-has->Article
    """
    def __init__(self, article_id, cited_by_article_id):
        self.article_id = int(article_id)
        self.cited_by_article_id = int(cited_by_article_id)


already_written_articles = {}


def parse_bibtex_files(fullpath, csv_article, csv_author, csv_article_has_author, csv_article_has_article):
    """
    open *.paper file and parse with BibTextParser
    :param fullpath:
    :return:
    """

    def write_article(article):
        """
        this helper method writes the Article instance to csv and tracks already written articles
        :param article:
        :return:
        """
        if not article.isi in already_written_articles:
            csv_article.writerow(article.__dict__.values())
            already_written_articles[article.isi] = article
            return article
        else:
            return already_written_articles[article.isi]

    def write_author(author):
        """
        this helper method writes the Author instance to csv
        :param author:
        :return:
        """
        csv_author.writerow(author.__dict__.values())
        return author

    def write_article_authors(article, authors):
        """
        this helper method writes the Article and their Author relationship to csv, based on their ids
        :param article:
        :param authors:
        :return:
        """
        for author in authors:
            author = write_author(author)
            csv_article_has_author.writerow(ArticleHasAuthor(article.id, author.id).__dict__.values())

    def write_article_has_article(article, cited_by_article):
        """
        this helper method writes the Article and their cited_by Article relationship to csv, based on their ids
        :param article:
        :param cited_by_article:
        :return:
        """
        csv_article_has_article.writerow(ArticleHasArticle(article.id, cited_by_article.id).__dict__.values())

    #fetch all bibtex entries
    bibtex_entries = utils.get_bibtex_entry(fullpath)

    assert len(bibtex_entries) == 1, "Critical error in file %s - to many bibtex entries!!!"%(fullpath)

    #get the bibtex entry and the isi
    isi, bibtex_entry = bibtex_entries.popitem()

    #get Article instance
    article = Article.get_instance_by_bibtex(bibtex_entry)
    article = write_article(article)

    #get Author instances
    authors = Author.get_instances_by_bibtex(bibtex_entry)
    write_article_authors(article, authors)

    #get citing Article instance
    cited_by_isi_entries = utils.get_cited_by_bibtex_entry(isi)

    #now get all citing articles, and write them also to csv
    for cited_by_isi, cited_entry in cited_by_isi_entries.items():
        cited_by_article = Article.get_instance_by_bibtex(cited_entry)
        cited_by_article = write_article(cited_by_article)

        write_article_has_article(article, cited_by_article)

        cited_by_authors = Author.get_instances_by_bibtex(cited_entry)
        write_article_authors(cited_by_article, cited_by_authors)

if __name__ == '__main__':

    start = time.time()

    #delete old csv directory
    if os.path.exists(settings.CSV_FILE_OUTPUT):
        shutil.rmtree(settings.CSV_FILE_OUTPUT)

    os.mkdir(settings.CSV_FILE_OUTPUT)

    #first create all csv files
    f_articles = open(os.path.join(settings.CSV_FILE_OUTPUT, 'article.csv'), 'wb')
    csv_articles =csv.writer(f_articles, delimiter=',', quotechar='"')
    csv_articles.writerow(Article().__dict__.keys())

    f_authors = open(os.path.join(settings.CSV_FILE_OUTPUT, 'author.csv'), 'wb')
    csv_authors = csv.writer(f_authors, delimiter=',', quotechar='"')
    csv_authors.writerow(Author().__dict__.keys())

    f_article_has_author = open(os.path.join(settings.CSV_FILE_OUTPUT, 'article_has_author.csv'), 'wb')
    csv_article_has_author =csv.writer(f_article_has_author, delimiter=',', quotechar='"')
    csv_article_has_author.writerow(ArticleHasAuthor(0, 0).__dict__.keys())

    f_article_has_article = open(os.path.join(settings.CSV_FILE_OUTPUT, 'article_has_article.csv'), 'wb')
    csv_article_has_article =csv.writer(f_article_has_article, delimiter=',', quotechar='"')
    csv_article_has_article.writerow(ArticleHasArticle(0, 0).__dict__.keys())

    #iterate over all bibtex files
    files = utils.get_paper_filepaths()
    for i in range(len(files)):
        utils.update_progress(i+1, len(files))

        try:
            parse_bibtex_files(files[i], csv_articles, csv_authors, csv_article_has_author, csv_article_has_article)
        except Exception as e:
            print "-----------parse_bibtex_files exception----------------"
            print e
            print "-----------skipping: %s ------------------------"%files[i]


    #close the file handles
    f_articles.close()
    f_authors.close()
    f_article_has_author.close()
    f_article_has_article.close()

    print "\n"+"%s successful finished in: %i seconds" % (os.path.basename(__file__),(time.time() - start))


