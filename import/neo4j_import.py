import os
import time
import sys
import settings
from py2neo import neo4j
from string import Template
from utils import utils

__author__ = 'Rudolf'

def split_csv_file_into_parts(import_file):
    """
    since importing has been a problem with to big data sets, this function splits the csv into smaller parts
    see the settings.CSV_MAX_ROWS setting which determines when to split a specific csv file
    :param import_file:
    :return:
    """
    with open(import_file) as f:
        header_line=f.readline()

    line_buffer = []
    iteration=0
    for line in open(import_file):
        line_buffer.append(line)
        #flush buffers to file
        if len(line_buffer) >= settings.CSV_MAX_ROWS:
            open(import_file+"_"+str(iteration), 'wb').writelines(line_buffer)
            line_buffer = [header_line]
            iteration+=1

    if len(line_buffer) > 0:
        open(import_file+"_"+str(iteration), 'wb').writelines(line_buffer)
        iteration+=1

    return [import_file+"_"+str(i) for i in range(0, iteration)]


def run_query(graph_db, query):
    """
    execute the query
    :param graph_db:
    :param query:
    :return:
    """

    start = time.time()
    print "running query: %s" %(query)
    neo4j.CypherQuery(graph_db, query).run()
    print "-------query successful finished in: %i seconds---------\n" % (time.time() - start)

def run_query_file(graph_db, query_file, **kwargs):
    """
    execute the query from query_file with kwargs arguments
    :param graph_db:
    :param query_file:
    :param kwargs:
    :return:
    """

    print "running query of file: %s" %(query_file)
    query = open(query_file).read()
    query = Template(query)
    query = query.substitute(**kwargs)

    run_query(graph_db,query)



if __name__ == '__main__':
    """ import data in neo4j """

    start = time.time()
    #change current working directory to this file directory
    os.chdir(os.path.dirname(__file__))

    def get_csv_path(csv_path):
        return (settings.CSV_FILE_OUTPUT+ "\\"+csv_path).replace("\\","/")

    graph_db = neo4j.GraphDatabaseService(settings.NEO4J_SERVER_DATA_URL)

    ARRAY_JOIN_CHARACTER=settings.ARRAY_JOIN_CHARACTER

    run_query(graph_db, "CREATE INDEX ON :Article(id)")

    run_query(graph_db, "CREATE INDEX ON :Author(id)")


    for file in  split_csv_file_into_parts(get_csv_path("article.csv")):
        run_query_file(graph_db,"./../query/import/import_article.cql", IMPORT_FILE=file, ARRAY_JOIN_CHARACTER=ARRAY_JOIN_CHARACTER)

    for file in  split_csv_file_into_parts(get_csv_path("author.csv")):
        run_query_file(graph_db,"./../query/import/import_author.cql", IMPORT_FILE=file, ARRAY_JOIN_CHARACTER=ARRAY_JOIN_CHARACTER)

    for file in  split_csv_file_into_parts(get_csv_path("article_has_author.csv")):
        run_query_file(graph_db,"./../query/import/import_article_has_author.cql", IMPORT_FILE=file, ARRAY_JOIN_CHARACTER=ARRAY_JOIN_CHARACTER)

    for file in  split_csv_file_into_parts(get_csv_path("article_has_article.csv")):
        run_query_file(graph_db,"./../query/import/import_article_has_article.cql", IMPORT_FILE=file, ARRAY_JOIN_CHARACTER=ARRAY_JOIN_CHARACTER)

    # run_query_file(graph_db,"./../query/import/update_author_cites.cql", LOWER_BOUND ="0.0", UPPER_BOUND = "1.0")

    # sys.exit(0)


    """
    convert (article) keywords single string -> keywords string array
    calculate keywords, keywords_plus, keywords_all union / intersect
    """
    run_query_file(graph_db,"./../query/import/update_keywords_string_to_array.cql", ARRAY_JOIN_CHARACTER=ARRAY_JOIN_CHARACTER)

    run_query_file(graph_db,"./../query/import/update_keywords.cql", KEYWORD="keywords")
    run_query_file(graph_db,"./../query/import/update_keywords.cql", KEYWORD="keywords_plus")

    run_query_file(graph_db,"./../query/import/update_article_keywords_all.cql")
    run_query_file(graph_db,"./../query/import/update_keywords.cql", KEYWORD="keywords_all")

    run_query_file(graph_db,"./../query/import/update_keywords_jaccard.cql", KEYWORD="keywords_plus")
    run_query_file(graph_db,"./../query/import/update_keywords_jaccard.cql", KEYWORD="keywords")
    run_query_file(graph_db,"./../query/import/update_keywords_jaccard.cql", KEYWORD="keywords_all")

    run_query_file(graph_db,"./../query/import/update_author_with_authors_list.cql")
    run_query_file(graph_db,"./../query/import/update_author_with_authors_jaccard.cql")


    # """
    # create (author) -[cited_by]-> (author)
    # """

    #for (lower_bound, upper_bound) in utils.generate_lower_upper_bounds(settings.QUERY_WHERE_ISI_SPLIT_AFTER_PERCENT):
    #    print "running query with bounds: %s, %s"%(lower_bound, upper_bound)
    #    run_query_file(graph_db,"./../query/import/create_rel_cited_by.cql", LOWER_BOUND = str(lower_bound), UPPER_BOUND = str(upper_bound))

    #
    # run_query_file(graph_db,"./../query/import/update_author_cited_by_count.cql")
    # run_query_file(graph_db,"./../query/import/update_author_cited_by_count_sum.cql")
    # run_query_file(graph_db,"./../query/import/update_author_cited_by_reverse.cql")
    # run_query_file(graph_db,"./../query/import/update_author_cited_by_reverse2.cql")
    #
    #
    # """
    # create _back_after_citation link metrics
    # """
    #
    # run_query_file(graph_db,"./../query/import/update_reci_after_citations.cql")
    # run_query_file(graph_db,"./../query/import/update_reci_before_citations.cql")


    print "%s successful finished in: %i seconds" % (os.path.basename(__file__),(time.time() - start))
