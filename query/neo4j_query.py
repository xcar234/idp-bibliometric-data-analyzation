import csv
import os
import time
from string import Template
import sys

from py2neo import neo4j

import settings
from utils import utils

__author__ = 'Rudolf'
def main():
    """
    Runs a query against the neo4j database and saves it in a .csv file.
    This script uses the author id as index and substitutes the AUTHOR_ID variable in the query template file.
    This allows to process very large query result sets.
    """
if __name__ == '__main__':
    start = time.time()
    # change current working directory to this file directory
    os.chdir(os.path.dirname(__file__))
    print "current working directory: " + os.getcwd()

    query_file = sys.argv[1]
    print "using query_file: " + query_file

    match_query_template = open(query_file).read()

    graph_db = neo4j.GraphDatabaseService(settings.NEO4J_SERVER_DATA_URL)
    query = neo4j.CypherQuery(graph_db, "MATCH (n:Author) RETURN DISTINCT n.id")

    f_out = open(os.path.join("output.csv"), 'wb')
    csv_out = csv.writer(f_out, delimiter=',', quotechar='"')
    print "csv_out: " + os.path.join("output.csv")

    header_printed = False

    authors_records = query.execute()

    # for each author
    for i in range(len(authors_records)):
        utils.update_progress(i + 1, len(authors_records))

        author_id = authors_records[i][0]
        match_query = Template(match_query_template).substitute(AUTHOR_ID=author_id)

        #for each match query result
        for match_record in neo4j.CypherQuery(graph_db, match_query).stream():
            if not header_printed:
                csv_out.writerow(match_record.columns)
                header_printed = True

            csv_out.writerow(match_record.values)

    f_out.close()

    print "%s successful finished in: %i seconds" % (os.path.basename(__file__), (time.time() - start))
