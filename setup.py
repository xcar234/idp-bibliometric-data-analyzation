#!/usr/bin/env python

from setuptools import setup

setup(name='IDP-Python',
      version='0.2',
      description='IDP - parsing papers & citations into Neo4j',
      author='Rudolf M.',
      author_email='phazz@online.de',
      url='http://',
      install_requires=[
          "bibtexparser",
          "py2neo",
          "pyreadline",
          "fabric",
          "boto",

      ],
)