#!/bin/bash
#SBATCH -o /home/hpc/pr87de/di73gux/myjob.%j.%N.out
#SBATCH -D /home/hpc/pr87de/di73gux/idp
#SBATCH -J graph_database_analyze
#SBATCH --get-user-env
#SBATCH --clusters=uv3
#SBATCH --nodes=1-1
#SBATCH --cpus-per-task=128
#SBATCH --mail-type=end
#SBATCH --mail-user=mathey@in.tum.de
#SBATCH --export=NONE
#SBATCH --time=24:00:00
##source /etc/profile.d/modules.sh

export OMP_NUM_THREADS=128

cd /home/hpc/pr87de/di73gux/idp/IDP-Python
source ./scripts/settings

sh ./neo4j.sh start && echo "sleep 3min" && sleep 3m && wget localhost:${NEO4J_PORT} \
&& bash ./scripts/query.sh     \
&& sh ./neo4j.sh stop || sh ./neo4j.sh stop











