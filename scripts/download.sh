#!/bin/bash

mkdir ~/idp
cd ~/idp
wget http://www.configurator.wi.tum.de/citReci/datasetC00/paper.zip -O paper.zip
unzip -q paper.zip

wget http://www.configurator.wi.tum.de/citReci/datasetC00/citedby.zip -O citedby.zip
unzip -q citedby.zip