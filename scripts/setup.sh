#!/bin/bash

source ./scripts/settings

mkdir ${MY_HOME}/idp
cd  ${MY_HOME}/idp

#download python 2.7
wget https://www.python.org/ftp/python/2.7.8/${PATH_PYTHON}.tar.xz -O ${PATH_PYTHON}.tar.xz
tar -xvf ${PATH_PYTHON}.tar.xz

#setup python
cd ${PATH_PYTHON}
./configure --prefix=${MY_HOME}/.local
make
make install
wget https://bootstrap.pypa.io/ez_setup.py -O - | ${MY_HOME}/.local/bin/python

${MY_HOME}/.local/bin/easy_install pip

#download java jdk7
wget --no-check-certificate --no-cookies --header "Cookie: oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jdk/7u67-b01/${PATH_JAVA}.tar.gz -O ${PATH_JAVA}.tar.gz

tar -xvf ${PATH_JAVA}.tar.gz

#download neo4j
wget http://neo4j.com/artifact.php?name=${PATH_NEO4J}-unix.tar.gz -O ${PATH_NEO4J}.tar.gz

tar -xvf ${PATH_NEO4J}.tar.gz

#setup neo4j

cd ${PATH_NEO4J}/conf
#echo "org.neo4j.server.webserver.address=0.0.0.0" | tee -a neo4j-server.properties
#echo "neostore.nodestore.db.mapped_memory=190M" | tee -a neo4j.properties
#echo "neostore.relationshipstore.db.mapped_memory=3G" | tee -a neo4j.properties
#echo "neostore.propertystore.db.mapped_memory=500M" | tee -a neo4j.properties
#echo "neostore.propertystore.db.strings.mapped_memory=1G" | tee -a neo4j.properties
#echo "neostore.propertystore.db.arrays.mapped_memory=500M" | tee -a neo4j.properties

echo "node_auto_indexing=true" | tee -a neo4j.properties
echo "node_keys_indexable=id,isi,name,title,author" | tee -a neo4j.properties

echo "relationship_auto_indexing=true" | tee -a neo4j.properties
echo "relationship_keys_indexable=id,isi,name,title,author" | tee -a neo4j.properties

echo "node_keys_indexable=id,isi,name,title,author" | tee -a neo4j.properties

#echo "wrapper.java.initmemory=10000" | tee -a neo4j-wrapper.conf
#echo "wrapper.java.maxmemory=64000" | tee -a neo4j-wrapper.conf

echo "dbms.security.authorization_enabled=false" | tee -a neo4j-server.properties


#setup setup.py from project
cd  ${MY_HOME}/idp/IDP-Python
${MY_HOME}/.local/bin/python setup.py install

/usr/bin/yes | ${MY_HOME}/.local/bin/pip uninstall pyreadline
