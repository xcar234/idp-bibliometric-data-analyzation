#!/bin/bash

LIMIT=1000000
STEPS=500
i=0

#with skip loop
while [  $i -lt $STEPS ]; do
	SKIP="$(($i * $LIMIT))"
	
	#query without skip
	if [ $i -eq 0 ]
	then
		sed -e "s/\${_skip_limit_templ}/LIMIT ${LIMIT}/" query/query.json > query_concrete_${i}.json
	#query with skip		
	else
		sed -e "s/\${_skip_limit_templ}/SKIP ${SKIP} LIMIT ${LIMIT}/" query/query.json > query_concrete_${i}.json
	fi
		curl -d@query_concrete_${i}.json -H X-Stream:true -H Content-Type:application/json http://localhost:7474/db/data/cypher > stream_${i}.json
	
let i=i+1
done

wait

echo "done"