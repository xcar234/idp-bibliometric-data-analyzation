python C:\Python27\Scripts\pdoc import.neo4j_import --html --html-dir docs --overwrite
python C:\Python27\Scripts\pdoc import.bibtex_to_csv_parser --html --html-dir docs --overwrite
python C:\Python27\Scripts\pdoc utils.bibtex_utils --html --html-dir docs --overwrite
python C:\Python27\Scripts\pdoc utils.utils --html --html-dir docs --overwrite
python C:\Python27\Scripts\pdoc test.statistics --html --html-dir docs --overwrite
python C:\Python27\Scripts\pdoc query.neo4j_query --html --html-dir docs --overwrite