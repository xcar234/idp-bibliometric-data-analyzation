__author__ = 'Rudolf'

import sys
import re
import os

from bibtexparser import customization
from bibtexparser.bparser import BibTexParser

import bibtex_utils
import settings


def update_progress(i, end_val, bar_length=20):
    """
    updates the command line with the specified progess status
    :param i: progess value
    :param end_val: max progress value
    :param bar_length: the bar length in lines
    :return:
    """
    # for i in xrange(0, end_val):
    percent = float(i) / end_val
    hashes = '#' * int(round(percent * bar_length))
    spaces = ' ' * (bar_length - len(hashes))
    sys.stdout.write("\rPercent: [{0}] {1}%, {2} of {3}".format(hashes + spaces, int(round(percent * 100)), i, end_val))
    sys.stdout.flush()


def get_bibtex_entry(bibfile_path):
    """
    parses bibtex file and returns an Dict
    :param bibfile_path: path of the bibfile
    :return: dictionary of the bibtex entry at the bibfile_path
    """

    with open(bibfile_path, 'r') as bibfile:
        content = bibfile.read().strip()
        content = re.sub(r"\}\s*@", "}\n@", content)

        bp = BibTexParser(content)
        bibtex_entries = bp.get_entry_dict()

        """ parse the bibtex file and customize/normalize some values
        e.g.: big keywords string -to-> keywords python list """
        for isi, values in bibtex_entries.items():
            values = customization.author(values)
            values = customization.keyword(values)
            values = bibtex_utils.keywords_plus(values)
            values = bibtex_utils.keyword_normalizer(values)
            values = bibtex_utils.keywords_to_upper(values)
            values = bibtex_utils.affiliation(values)

        return bibtex_entries


def get_paper_filepaths():
    """
    walk through directory and visit all files ending with *.paper
    :return: list of file paths
    """
    file_list = []
    for root, _, files in os.walk(settings.PAPER_FILE_PATH):
        for f in files:

            if not f.endswith(settings.PAPER_FILE_ENDING):
                print ", skipping %s, reason: not ending with %s" % (f, settings.PAPER_FILE_ENDING)
                continue

            fullpath = os.path.join(root, f)
            file_list.append(fullpath)

    return file_list


def get_cited_by_bibtex_entry(isi_key):
    """
    get and parse cited_by file for specific isi_key, resolve all isi_properties that cited isi_key
    :param isi_key: the isi identifier key to find the cited_by file
    :return: dictionary of all bibtex entries that cited isi_key
    """

    if ":" in isi_key:
        isi_nr = isi_key.split(":")[1]

    cited_by_file_path = os.path.join(settings.CITED_BY_FILE_PATH, str(isi_nr) + settings.CITED_BY_FILE_ENDING)

    if not os.path.exists(cited_by_file_path):
        print ", skipping, file does not exist: %s" % (cited_by_file_path)
        return {}
    else:
        bibtex_entries = get_bibtex_entry(cited_by_file_path)
        return bibtex_entries


import decimal
from math import ceil
# find best range function available to version (2.7.x / 3.x.x)
try:
    _xrange = xrange
except NameError:
    _xrange = range

def drange(start, stop = None, step = 1, precision = None):
    """drange generates a set of Decimal values over the
    range [start, stop) with step size step

    drange([start,] stop, [step [,precision]])"""

    if stop is None:
        for x in _xrange(int(ceil(start))):
            yield x
    else:
        # find precision
        if precision is not None:
            decimal.getcontext().prec = precision
        # convert values to decimals
        start = decimal.Decimal(start)
        stop = decimal.Decimal(stop)
        step = decimal.Decimal(step)
        # create a generator expression for the index values
        indices = (
            i for i in _xrange(
                0,
                ((stop-start)/step).to_integral_value()
            )
        )
        # yield results
        for i in indices:
            yield float(start + step*i)



def generate_lower_upper_bounds(step=0.1):
    """
    generates lower and upper bounds with values between 0.0 <= x <= 1.0
    :param step:
    :return:
    """
    steps = list(drange(0,1,step))
    lower_upper_bounds = []

    for i in range(0,len(steps)):
        lower_bound = str(steps[i])
        if i >=len(steps)-1:
            upper_bound = "1.0"
        else:
            upper_bound = str(steps[i+1])

        lower_upper_bounds.append((lower_bound,upper_bound))

    return lower_upper_bounds



def levenshtein(a,b):
    """
    Calculates the Levenshtein distance between a and b.
    :param a:
    :param b:
    :return:
    """
    n, m = len(a), len(b)
    if n > m:
        # Make sure n <= m, to use O(min(n,m)) space
        a,b = b,a
        n,m = m,n

    current = range(n+1)
    for i in range(1,m+1):
        previous, current = current, [i]+[0]*n
        for j in range(1,n+1):
            add, delete = previous[j]+1, current[j-1]+1
            change = previous[j-1]
            if a[j-1] != b[i-1]:
                change = change + 1
            current[j] = min(add, delete, change)

    return current[n]


def similarity_index(a,b):
    """
    Calculates the similarity index based on the Levenshtein distance between a and b.
    :param a:
    :param b:
    :return:
    """
    distance = 0 if a == b else levenshtein(a,b)

    #calculates the middle of distance/len(a) and distance/len(b)
    return (float(distance)/(len(a)+1.0) + (float(distance)/(len(b)+1.0))) /2.0