import re

__author__ = 'Rudolf'


def keyword_normalizer(record):
    """
    renames the property "keyword" to "keywords" to provide a consistent interface to the data
    :return:
    """

    if "keyword" in record:
        record["keywords"] = record["keyword"]
        del record["keyword"]

    if "keyword_plus" in record:
        record["keywords_plus"] = record["keyword_plus"]
        del record["keyword_plus"]

    return record

def keywords_to_upper(record):
    """
    converts all keywords to upper, for same formating!
    :return:
    """

    if "keywords" in record:
        if type(record["keywords"]) is list:
            record["keywords"] = [keyword.upper().strip() for keyword in record["keywords"]]

    if "keywords_plus" in record:
        if type(record["keywords_plus"]) is list:
            record["keywords_plus"] = [keyword.upper().strip() for keyword in record["keywords_plus"]]

    return record

def keywords_plus(record, sep=',|;'):
    """
    Split keyword-plus field into a list.

    :param record: the record.
    :type record: dict
    :param sep: pattern used for the splitting regexp.
    :type record: string, optional
    :return: dict -- the modified record.

    """
    if "keywords-plus" in record:
        record["keywords-plus"] = [i.strip() for i in re.split(sep, record["keywords-plus"].replace('\n', ''))]

    return record

def affiliation(record, sep='(\w{2,}\.+[^;,])'):
    """
    creates an affiliation array to provide a consistent interface to the data
    uses a regex seperator: sep='(\w{2,}\.+[^;,])'
    try regex with: http://regex.larsolavtorvik.com/ and any affiliation data
    :return:
    """

    if "affiliation" in record:
        affiliations = [i.strip() for i in re.split(sep, record["affiliation"].replace('\n', '')) if i.strip()]

        affiliations_new = []
        # after splitting the affiliation string, we want to keep the country of affiliation, so we add every 2nd entry here back again to the 1st
        for i in range(0,len(affiliations)):
            if i % 2 == 0:
                affiliations_new.append(affiliations[i])
            else:
                tail = affiliations_new.pop()
                tail += " "+affiliations[i]

                #normalize data, remove multiple spaces & dots
                tail = re.sub(r'\.', " ", tail.strip())
                tail = re.sub(r'\.+$', "", tail.strip())

                tail = re.sub(r'\s+', " ", tail.strip())

                affiliations_new.append(tail.strip())

        record["affiliation"] = affiliations_new

    return record
