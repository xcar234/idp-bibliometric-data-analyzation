import os

from testutils import utils


__author__ = 'Rudolf'

import unittest
import settings


class TestUtils(unittest.TestCase):
    def setUp(self):
        settings.CITED_BY_FILE_PATH = os.path.join(os.path.dirname(__file__), "data")
        settings.CITED_BY_FILE_ENDING = ".cb"

        settings.PAPER_FILE_PATH = os.path.join(os.path.dirname(__file__), "data")
        settings.PAPER_FILE_ENDING = ".paper"

        self.test_get_paper_file_paths()

    def test_get_paper_file_paths(self):

        self.files = utils.get_paper_filepaths()
        self.assertIn(os.path.join(settings.PAPER_FILE_PATH, "000309796000003" + settings.PAPER_FILE_ENDING),
                      self.files)

    def test_get_bibtex_entry(self):
        for file in self.files:
            key, bibtex_entry = utils.get_bibtex_entry(file).items().pop()
            self.assertIsNotNone(bibtex_entry)
            self.assertEqual(key, bibtex_entry["id"])
            self.assertIn(os.path.basename(file).replace(settings.PAPER_FILE_ENDING, ""), bibtex_entry["id"])

    def test_get_cited_by_bibtex_entry(self):
        for file in self.files:
            key, bibtex_entry = utils.get_bibtex_entry(file).items().pop()
            self.assertIsNotNone(bibtex_entry)

            cited_by_bibtex_entries = utils.get_cited_by_bibtex_entry(bibtex_entry["id"])
            self.assertGreater(len(cited_by_bibtex_entries), 0)


if __name__ == '__main__':
    unittest.main()