import os
import time
import settings
from utils import utils

__author__ = 'R'

isi_list = []
cited_by_isi_list = []

def parse_bibtex_files(fullpath):
    """
    open *.paper file and parse with BibTextParser
    :param fullpath:
    :return:
    """
   #fetch all bibtex entries
    bibtex_entries = utils.get_bibtex_entry(fullpath)

    if len(bibtex_entries) > 1:
        print "Critical error in file %s - to many bibtex entries!!!"

    #get the bibtex entry and the isi
    isi, bibtex_entry = bibtex_entries.popitem()
    isi_list.append(isi)

    cited_by_isi_entries = utils.get_cited_by_bibtex_entry(isi)

    #now get all citation articles, and write them also to csv
    for cited_by_isi, cited_entry in cited_by_isi_entries.items():
        cited_by_isi_list.append(cited_by_isi)


if __name__ == '__main__':

    start = time.time()

    #iterate over all bibtex files
    files = utils.get_paper_filepaths()
    for i in range(len(files)):
        utils.update_progress(i, len(files))

        parse_bibtex_files(files[i])

        #print statistics
    print ""
    print "isis count: %i" % len(isi_list)
    print "unique isis count: %i" % len(set(isi_list))
    print "cited_by isi count: %i" % len(cited_by_isi_list)
    print "unique cited_by isis count: %i" % len(set(cited_by_isi_list))
    print "unique isi + cited_by isi count: %i" % len(set(isi_list+cited_by_isi_list))

    print "\n"+"%s successful finished in: %i seconds" % (os.path.basename(__file__), (time.time() - start))

