#!/usr/bin/python
# -*- encoding: utf-8 -*-
import os
__author__ = 'Rudolf'

""" General settings """
PAPER_FILE_PATH = "/tmp/idp/paper"

CITED_BY_FILE_PATH = "/tmp/idp/cited_by"

CSV_FILE_OUTPUT = "/tmp/idp/csv"